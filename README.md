# README #

### SmartMage ExtraShippingMethods ###

Moduł dodaje 6 uniwersalnych metody dostawy. Można określić minimalną i maksymalną wartość zamówienia, oraz kwotę od której dostawa jest darmowa.
Konfiguracja znajduje się w Sklepy -> Konfiguracja -> Sprzedaż -> Metody dostawy -> Smartmage ExtraShippingMethods

### Instalacja ###

Pliki moduły wgrywamy do katalogu /app/code/Smartmage/ExtraShippingMethods

Następnie w konsoli uruchamiamy następujące komendy:

* php bin/magento module:enable Smartmage_ExtraShippingMethods
* php bin/magento setup:upgrade
* php bin/magento setup:di:compile
* php bin/magento setup:static-content:deploy pl_PL
* php bin/magento cache:flush

Działanie modułu można przetestować na naszej wersji demonstracyjnej sklepu dostępnej pod adresem http://demo.m2.smartmage.pl/