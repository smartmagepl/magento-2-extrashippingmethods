<?php

namespace Smartmage\ExtraShippingMethods\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;

class Extrashipping extends AbstractCarrier implements CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'smesm';

    /**
     * @var bool
     */
    protected $_isFixed = true;

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $_rateResultFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $_rateMethodFactory;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param array $data
     */
    public function __construct(
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        array $data = []
    ) {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    public function collectRates(RateRequest $request)
    {
        if(!$this->getConfigFlag('active')){
            return false;
        }

        $result = $this->_rateResultFactory->create();

        $discount = 0;

        if($request->getPackageValue() != $request->getPackageValueWithDiscount()){
            $discount = $request->getPackageValue() - $request->getPackageValueWithDiscount();
        }

        $total_price = $request->getBaseSubtotalInclTax() - $discount;

        $packageValue = $request->getBaseCurrency()->convert($total_price, $request->getPackageCurrency());

        for($i = 0; $i <= 6; $i++){
            $check_countries = $this->checkAvailableShipMethodCountries($request, $i);
            if(!$this->getConfigFlag('active_'.$i) || false === $check_countries){
                continue;
            }

            if($this->getConfigData('type_'.$i) == 'I'){ // per item
                $shippingPrice = ($request->getPackageQty() * $this->getConfigData('price_'.$i)) - ($this->getFreeBoxes() * $this->getConfigData('price_'.$i));
            } else {
                $shippingPrice = $this->getConfigData('price_'.$i);
            }

            $free_shipping_from = $this->getConfigData('free_shipping_from_'.$i);
            if($free_shipping_from > 0 && $packageValue > $free_shipping_from){
                $shippingPrice = 0;
            }

            $shippingName = $this->getConfigData('name_'.$i);
            if($shippingName != '' && ($packageValue >= $this->getConfigData('order_amount_from_'.$i) && $packageValue <= $this->getConfigData('order_amount_to_'.$i)) || $shippingName != '' && $this->getConfigData('order_amount_to_'.$i) == ''){
                $method = $this->_rateMethodFactory->create();
                $method->setCarrier($this->_code);
                $method->setCarrierTitle($this->getConfigData('title'));
                $method->setMethod($this->_code.'_'.$i);
                $method->setMethodTitle($this->getConfigData('name_'.$i));
                $method->setPrice($shippingPrice);
                $method->setCost($shippingPrice);
                $result->append($method);
            }
        }

        return $result;
    }

    public function checkAvailableShipMethodCountries(\Magento\Framework\DataObject $request, $lp)
    {
        $speCountriesAllow = $this->getConfigData('sallowspecific_'.$lp);

        if($speCountriesAllow && $speCountriesAllow == 1){
            $availableCountries = [];
            if($this->getConfigData('specificcountry_'.$lp)){
                $availableCountries = explode(',', $this->getConfigData('specificcountry_'.$lp));
            }
            if($availableCountries && in_array($request->getDestCountryId(), $availableCountries)){
                return $this;
            } else {
                return false;
            }
        }

        return $this;
    }

    public function getAllowedMethods()
    {
        $arr = [];
        for($i = 1; $i <= 6; $i++){
        	if($this->getConfigData('active_'.$i) && $this->getConfigData('name_'.$i)){
            	$arr[$this->_code.'_'.$i] =  $this->getConfigData('name_'.$i);
			}
        }

        return $arr;
    }
}